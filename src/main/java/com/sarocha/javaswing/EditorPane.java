/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.javaswing;

import javax.swing.JEditorPane;
import javax.swing.JFrame;

/**
 *
 * @author Sarocha
 */
public class EditorPane {

    JFrame f = null;

    private void test() {
        f = new JFrame("JEditorPane Test");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(400, 200);
        JEditorPane myPane = new JEditorPane();
        myPane.setContentType("text/plain");
        myPane.setText("Sleeping is necessary for a healthy body."
                       + " But sleeping in unnecessary times may spoil our health, wealth and studies."
                       + " Doctors advise that the sleeping at improper timings may lead for obesity during the students days.");
        f.setContentPane(myPane);
        f.setVisible(true);
    }

    public static void main(String[] args) {
        (new EditorPane()).test();
    }
}
